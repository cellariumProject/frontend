#!/bin/bash


# Git Setup Multiple Repository
git remote add gitlab git@gitlab.com:cellarium/frontend.git
git remote set-url --add --push origin git@gitlab.com:cellarium/frontend.git

git remote add github git@github.com:Cellarium0/Frontend.git
git remote set-url --add --push origin git@github.com:Cellarium0/Frontend.git


# Display Config
git remote show origin

git config --list
