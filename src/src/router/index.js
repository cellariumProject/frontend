import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store";

import NotFoundView from "@/views/NotFoundView";
import HomeView from "@/views/HomeView";
import StatsView from "@/views/StatsView";
import StockView from "@/views/StockView";
import AccountPageView from "@/views/Account/AccountPageView";
import AccountLoginRegisterView from "@/views/Account/AccountLoginRegisterView";
import AccountChangePasswordView from "@/views/Account/AccountChangePasswordView";
import ShoppingListView from "@/views/ShoppingListView";
import ItemsListView from "@/views/ItemsListView";
import GroceryView from "@/views/GroceryView";

Vue.use(VueRouter);

const routes = [
  {
    path: "*",
    name: "NotFound",
    component: NotFoundView,
  },
  {
    path: "/",
    name: "Home",
    component: HomeView,
  },
  {
    path: "/account/login",
    name: "AccountLoginRegister",
    component: AccountLoginRegisterView,
  },
  {
    path: "/account",
    name: "AccountPage",
    component: AccountPageView,
    meta: { requiresAuth: true },
  },

  {
    path: "/account/change-password",
    name: "AccountChangePassword",
    component: AccountChangePasswordView,
    meta: { requiresAuth: true },
  },
  {
    path: "/stats",
    name: "Stats",
    component: StatsView,
    meta: { requiresAuth: true },
  },
  {
    path: "/stock",
    name: "Stock",
    component: StockView,
    meta: { requiresAuth: true },
  },
  {
    path: "/shopping-list",
    name: "ShoppingList",
    component: ShoppingListView,
    meta: { requiresAuth: true },
  },
  {
    path: "/items",
    name: "ItemsListView",
    component: ItemsListView,
    meta: { requiresAuth: true },
  },
  {
    path: "/grocery",
    name: "GroceryView",
    component: GroceryView,
    meta: { requiresAuth: true },
  },
];

const router = new VueRouter({
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (store.getters.isAuthenticated) {
      next();
      return;
    }
    next("/account/login");
  } else {
    next();
  }
});

export default router;
