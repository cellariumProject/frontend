import axios from "axios";
import store from "../store/index";
import router from "../router/index";

let httpService = {};
let http = axios;

http.defaults.baseURL =
  process.env.VUE_APP_API_URL === undefined ? "" : process.env.VUE_APP_API_URL;

const token = localStorage.getItem("token");

if (token) http.defaults.headers.common["Authorization"] = `Bearer ${token}`;

http.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if (error.response.status == 401) {
      store.dispatch("logout");
      return router.push("/account/login");
    }
    return Promise.reject(error);
  }
);

httpService.install = function (Vue) {
  Vue.prototype.$http = http;
};

export default httpService;
