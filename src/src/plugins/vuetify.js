import Vue from "vue";
import Vuetify from "vuetify/lib";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#0ca07b",
        secondary: "#4ad395",
        error: "#b71c1c",
        background: "#efefef",
        accent: "#d44b35",
      },
    },
  },
});
