# Cellarium : Frontend

[![pipeline status](https://gitlab.com/cellarium/frontend/badges/master/pipeline.svg)](https://gitlab.com/cellarium/frontend/-/commits/master)

![Icon](./icon.png)

## Table Of Contents

- [Cellarium : Frontend](#cellarium--frontend)
  - [Table Of Contents](#table-of-contents)
  - [Description](#description)
  - [Access](#access)
  - [Documentations](#documentations)
  - [Requirements](#requirements)
  - [Install](#install)
    - [Install : NPM](#install--npm)
    - [Install : Docker](#install--docker)
  - [Deploy](#deploy)
    - [Deploy : NPM](#deploy--npm)
    - [Deploy : Docker](#deploy--docker)
  - [Production Build](#production-build)
    - [Production Build : NPM](#production-build--npm)
    - [Production Build : Docker](#production-build--docker)
  - [Contributors](#contributors)
  - [Licence](#licence)

## Description

**Cellarium** is a web application to manage your food. This is the Frontend part.

## Access

- [Cellarium Frontend Development (Local)](http://localhost:8002)
- [Cellarium Frontend Production (Local)](http://localhost:8002)
- [Cellarium Frontend Production (Netlify)](https://cellarium.netlify.app/)

## Documentations

Here some documentation :

- [Command](./docs/command.md)
- [Idea](./docs/idea.md)

## Requirements

All requirements are in file **/src/package.json** :

- **Infrastructure** :
  - NPM
  - Docker
  - Docker Compose
  - Netlify
  - AWS ?
- **Framework** : VueJS
- **CSS Framework** : Vuetify
- **Request Engine** : Axios
- **Storage System** : Vuex Persisted State
- **Alert System** : SweetAlert2

## Install

### Install : NPM

    cd src
    npm install -g @vue/cli
    npm install

### Install : Docker

    docker-compose -f docker-compose.dev.yml build

## Deploy

### Deploy : NPM

    cd src

    # Synthax Check
    npx vue-cli-service lint

    # Start Server
    npx vue-cli-service serve

### Deploy : Docker

    # Development
    docker-compose -f docker-compose.dev.yml up
    docker-compose -f docker-compose.dev.yml up -d

    # Synthax Check
    docker-compose -f docker-compose.dev.yml run cellarium_frontend_dev npx vue-cli-service lint

    # Production
    docker-compose up
    docker-compose up -d

## Production Build

### Production Build : NPM

    cd src
    npx vue-cli-service build

### Production Build : Docker

    docker-compose build

## Contributors

- ProgOwer
- TODO

## Licence

This project is licensed under the terms of the GNU General Public Licence v3.0 and above licence.
