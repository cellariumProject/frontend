# Cellarium : Frontend : Command

![Icon](../icon.png)

## Table Of Contents

- [Cellarium : Frontend : Command](#cellarium--frontend--command)
  - [Table Of Contents](#table-of-contents)
  - [Command](#command)

## Command

    docker run -it --rm -v /home/src:/app -v node_modules:/app/node_modules node:latest bash

    # Get Vue CLI Tools
    npm install -g @vue/cli

    # Create Basic Application
    vue create -n app
    > Merge
    > Manual
    > Babel / Router / Vuex / Linter
    > 2.x
    > n
    > ESLint + Prettier
    > Lint on save
    > In package.json
    > N
    > Use NPM

    cd app

    # Install Vuetify
    vue add vuetify

    # Install Axios
    npm install axios --save

    # Install Vuex Persisted State
    npm install vuex-persistedstate --save

    # Install Vue SweetAlert2
    npm install vue-sweetalert2 --save

    # Fix Right Problem
    chmod 777 -R *

    # Check Synthax
    npx vue-cli-service lint

    # Run the server
    npx vue-cli-service serve
    npm run serve

    # Run Build
    npx vue-cli-service build
    npm run build
